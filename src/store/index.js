import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import vehicle from "./vehicle"
import hypothesis from "./hypothesis"

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
    key: 'vetool',
    storage: window.localStorage
})

export const store = new Vuex.Store({
    namespaced: true,
    state: {

    },
    modules: {
        hypothesis,
        vehicle
    },
    actions: {
        clear () {
            // alert('test');
            window.localStorage.removeItem('vetool');

            window.location.href = '/';
        }
    },
    plugins: [vuexPersist.plugin]
})
