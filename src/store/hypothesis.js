import * as types from '@/store/hypothesis-types';

export default {
    namespaced: true,
    state: {
        batteryEnergyPerKwh: 537.775, // Kwh required for / 1 Kwh storage
        co2PerKwhStorage: 0.236, // Kg of co2 per Kwh of energy required for battery production
        co2PerKwhElec: 0.050, // Kg of Co2 per Kwh of electricity produced
        chargeLoss: 10, // Percent of electricity loss while recharging
        co2PerLiterOfGas: 2.28, // Kg of co2 per liter of Gas
    },
    mutations: {
        replaceValue (state, {field, value}) {
            state[field] = value;
        },
    },
    actions: {
        [types.UPDATE_BATTERY_ENERGY_PER_KWH] (context, value) {
            context.commit('replaceValue', {field: 'batteryEnergyPerKwh', value})
        },
        [types.UPDATE_CO2_PER_KWH_SORAGE] (context, value) {
            context.commit('replaceValue', {field: 'co2PerKwhStorage', value})
        },
        [types.UPDATE_CO2_PER_KWH_ELEC] (context, value) {
            context.commit('replaceValue', {field: 'co2PerKwhElec', value})
        },
        [types.UPDATE_CHARGE_LOSS] (context, value) {
            context.commit('replaceValue', {field: 'chargeLoss', value})
        },
        [types.UPDATE_CO2_PER_LITER_OF_GAS] (context, value) {
            context.commit('replaceValue', {field: 'co2PerLiterOfGas', value})
        },
    }
}
