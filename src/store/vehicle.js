export default {
    namespaced: true,
    state: {
        vehicles: [
            {
                id: 0,
                name: 'Renault Zoe',
                co2Prod: 4500,
                batteryIncluded: false,
                type: 'BEV',
                batteryCapacity: 40,
                electricityConsumption: 15,
                onlyElectricUsage: 100,
                GasConsumption: 0,
            },
            {
                id: 1,
                name: 'Renault Clio',
                co2Prod: 6500,
                batteryIncluded: false,
                type: 'ICEV',
                batteryCapacity: null,
                electricityConsumption: null,
                onlyElectricUsage: 0,
                GasConsumption: 5.0,
            },
            {
                id: 2,
                name: 'BMW I3',
                co2Prod: 4500,
                batteryIncluded: false,
                type: 'PHEV',
                batteryCapacity: 33,
                electricityConsumption: 15,
                onlyElectricUsage: 85,
                GasConsumption: 7.0,
            },
            {
                id: 3,
                name: 'Tesla model S 100',
                co2Prod: 12204,
                batteryIncluded: true,
                type: 'BEV',
                batteryCapacity: 100,
                electricityConsumption: 19.5,
                onlyElectricUsage: 100,
                GasConsumption: 0,
            },
        ]
    },
    mutations: {
        addVehicle (state, vehicle) {
            vehicle.id = state.vehicles.length;
            state.vehicles.push(vehicle)
        },
        updateVehicle (state, vehicle) {
            state.vehicles.splice(vehicle.id, 1, vehicle)
        },
        removeVehicle (state, index) {
            state.vehicles.splice(index, 1)
        },
    },
    actions: {
        addVehicle (context, vehicle) {
            context.commit('addVehicle', vehicle)
        },
        updateVehicle (context, vehicle) {
            context.commit('updateVehicle', vehicle)
        },
        removeVehicle (context, index) {
            context.commit('removeVehicle', index)
        },
    },
}
