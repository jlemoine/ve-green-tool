/**
 * Calculate initial carbon emission from vehicle and battery build
 *
 * @param hypothesis
 * @param vehicle
 * @returns {number}
 */
export const initial = (hypothesis, vehicle) => {
  let batteryProduction = 0;
  let vehicleProduction = parseInt(vehicle.co2Prod || 0);

  // If the battery emission is included in base vehicle production, skip
  if (!vehicle.batteryIncluded) {
    batteryProduction = parseInt(vehicle.batteryCapacity || 0) * parseFloat(hypothesis.batteryEnergyPerKwh) * parseFloat(hypothesis.co2PerKwhStorage);
  }

  return batteryProduction + vehicleProduction
};

/**
 * Calculate distance related carbon emission
 *
 * @param hypothesis
 * @param vehicle
 * @param km in kilometers
 * @returns {number}
 */
export const distance = (hypothesis, vehicle, km) => {
  let gasolineEmission = parseFloat(vehicle.GasConsumption || 0);
  let electricityEmission = parseFloat(vehicle.electricityConsumption || 0);
  let electricRatio = parseInt(vehicle.onlyElectricUsage === null ? 100 : vehicle.onlyElectricUsage);

  gasolineEmission = gasolineEmission * parseFloat(hypothesis.co2PerLiterOfGas);
  electricityEmission = electricityEmission * parseFloat(hypothesis.co2PerKwhElec) * (1 + parseInt(hypothesis.chargeLoss) / 100);

  gasolineEmission = electricRatio === 100 ? 0 : gasolineEmission * (1 - electricRatio / 100);
  electricityEmission = electricRatio === 0 ? electricityEmission : electricityEmission * electricRatio / 100;

  return (gasolineEmission + electricityEmission) / 100 * km
};

export const yearly = (hypothesis, vehicle) => {
  return vehicle.co2Prod / 10
};

export const chartData = (hypothesis, vehicle, km) => {
  let data = [];
  let step = 10000;

  data.push(initial(hypothesis, vehicle));

  for (let i = 0; i < km; i += step) {
    data.push(distance(hypothesis, vehicle, step) + data[data.length-1])
  }

  return data
};
