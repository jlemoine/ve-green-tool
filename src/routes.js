import Home from './components/Home'
import Hypothesis from './components/Hypothesis'

export default [
    { path: '/hypothesis', name: 'hypothesis', component: Hypothesis },
    { path: '/:id?', name: 'home', component: Home }
]
