module.exports = {
  devServer: {
    disableHostCheck: true,
    sockHost: 'www.vetool.test'
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/ve-green-tool/'
    : '/'
}
